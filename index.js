// console.log("Hello World!")

// [SECTION] Synchronous vs Asynchronous

// Javascript, by default, is sychronous meaning it can only execute a block of code by line.
// It reads from top to bottom and left to right.

console.log("Hello World!")
// conosle.log("Hello Again")
console.log("Goodbye")

// [SECTION] Getting All Posts
// The Fetch API allows you to asynchronously request for a resource (data)
	// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value
	// Syntax
		// fetch('URL')

// to check if the connection or the server/url is still available or working.
console.log(fetch("https://jsonplaceholder.typicode.com/posts")
	.then(response => console.log(response.status)))

// use the .json method from the response object to convert data retrieved into JSON format
fetch("https://jsonplaceholder.typicode.com/posts")
	// response -> raw data
	// response.json -> converted data
	.then((response) => response.json())
	.then((json) => console.log(json)) 

// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
	// Used in functions to indicate which portions of code should be waited for
	// Creates an asynchronous function

async function fetchData() {

	// waits for the fetch method to complete then stores the value in the result variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts")

	console.log(result)
	console.log(typeof result)

	let json = await result.json()
	console.log(json)
}

fetchData()

// [SECTION] Getting a specific post

// retreives a specific post following the rest api (retrieves, /posts/:id, GET)
fetch("https://jsonplaceholder.typicode.com/posts/1").then((response) => response.json()).then((json) => console.log(json))

// [SECTION] Creating a post
fetch("https://jsonplaceholder.typicode.com/posts",
{
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World",
		userId: 1
	})
}).then((response) => response.json()).then((json) => console.log(json))

// [SECTION] Updating a post using PUT
fetch("https://jsonplaceholder.typicode.com/posts/1",
{
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		id: 1,
		title: "Updated Post",
		body: "Hello Again",
		userId: 1
	})
}).then((response) => response.json()).then((json) => console.log(json))

// [SECTION] Updating a post using PATCH
fetch("https://jsonplaceholder.typicode.com/posts/1",
{
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Corrected Post"
	})
}).then((response) => response.json()).then((json) => console.log(json))

// [SECTION] Deleting a post
fetch("https://jsonplaceholder.typicode.com/posts/1", {	method: "DELETE" })

// [SECTION] Filtering posts
fetch("https://jsonplaceholder.typicode.com/posts?userId=10").then((response) => response.json()).then((json) => console.log(json))